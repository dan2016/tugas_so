unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var a,b,c,x : integer;

begin
  memo1.Clear;
  x:=strtoint(edit1.Text);
  for A := 1 to x do
  begin
    C := 0;
    for B := 1 to A do
    begin
      if (A mod B = 0) then C := C + 1;
    end;
    if C = 2 then memo1.Lines.Add(inttostr(a));
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  showmessage('Terima Kasih');
  close;
end;

end.

